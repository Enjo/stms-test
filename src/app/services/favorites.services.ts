import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { HttpClient, HttpRequest } from "@angular/common/http";
import { FAVORITE } from "./../models/favorites.model";
import { first } from "rxjs/operators";
import {
  LoadFavorites,
  AddFavorite,
  RemoveFavorite,
  EditFavorite
} from "../store/actions/favorites.actions";
import { AppState } from "./../app.state";
import {
  FAKE_SERVER_ADDRESS,
  TABLE_NAME_OF_FAVORITES
} from "../common/constants/app.constants";

@Injectable()
export class FavoritesService {
  constructor(private http: HttpClient, private store: Store<AppState>) {}

  loadFavoritesObs() {
    return this.http.get(FAKE_SERVER_ADDRESS + TABLE_NAME_OF_FAVORITES);
  }

  loadFavorites() {
    this.http
      .get(FAKE_SERVER_ADDRESS + TABLE_NAME_OF_FAVORITES)
      .pipe(first())
      .subscribe((favorites: FAVORITE[]) => {
        this.store.dispatch(new LoadFavorites(favorites));
      });
  }

  addFavorite(fav: FAVORITE) {
    this.http
      .post(FAKE_SERVER_ADDRESS + TABLE_NAME_OF_FAVORITES, fav)
      .pipe(first())
      .subscribe((favorite: FAVORITE) => {
        this.store.dispatch(new AddFavorite(favorite));
      });
  }

  editFavorite(fav: FAVORITE) {
    this.http
      .put(FAKE_SERVER_ADDRESS + TABLE_NAME_OF_FAVORITES + "/" + fav.id, fav)
      .pipe(first())
      .subscribe((favorite: FAVORITE) => {
        this.store.dispatch(new EditFavorite(favorite));
      });
  }

  deleteFavorite(fav: FAVORITE) {
    this.http
      .delete(FAKE_SERVER_ADDRESS + TABLE_NAME_OF_FAVORITES + "/" + fav.id)
      .pipe(first())
      .subscribe((favorite: FAVORITE) => {
        this.store.dispatch(new RemoveFavorite(fav));
      });
  }
}
