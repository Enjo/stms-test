import { Injectable } from "@angular/core";
import { HttpClient, HttpRequest } from "@angular/common/http";
import { map } from "rxjs/operators";
import { PROVIDER_DESC } from 'src/app/common/constants/providers.constants';
import { PROVIDER } from 'src/app/common/constants/providers.enum';
import Mappers from 'src/app/common/mappers/mappers';

@Injectable()
export class GistService {
  constructor(private http: HttpClient) {}

  public getIMDBData(value: string) {
    return this.http.get(PROVIDER_DESC[PROVIDER.IMDB].api_link + value);
  }

  public getCountriesData() {
    return this.http.get(PROVIDER_DESC[PROVIDER.COUNTRY].api_link).pipe(
      map((x: any) => {
        return Mappers.countryMapper(x);
      })
    );
  }
}
