import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import {
  MAIN_SIDEBAR_MENU_ITEMS,
  MENU_ITEMS
} from "../app/common/constants/sidebar-menu-items.constants";
import { AppState } from "./app.state";
import { FavoritesService } from './services/favorites.services';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  public MENU_ITEMS = MENU_ITEMS;
  public MAIN_SIDEBAR_MENU_ITEMS = MAIN_SIDEBAR_MENU_ITEMS;
  public favoritesLength: number = 0;

  constructor(private store: Store<AppState>, private service: FavoritesService,) {
    this.service.loadFavorites();

    this.store.select("favoritesPage").subscribe(data => {
      this.favoritesLength = data.favorites.length;
    });
  }
}
