import { NO_POSTER_IMAGE_URL } from "../constants/app.constants";

export default class Mappers {
  static imdbMapper = (x: any[]) =>
    x.map(x => ({
      title: x.Title,
      year: x.Year,
      poster: x.Poster === "N/A" ? NO_POSTER_IMAGE_URL : x.Poster,
      id: x.imdbID
    }));

  static countryMapper = (x: any[]) =>
    x.map(x => ({
      name: x.Name,
      region: x.Region,
      area: x.Area,
      currency_name: x.CurrencyName,
      flag: x.FlagPng,
      id: x.id
    }));
}
