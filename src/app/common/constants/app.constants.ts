
export const NO_POSTER_IMAGE_URL = "https://img.icons8.com/cotton/64/000000/film-reel.png";
export const FAKE_SERVER_ADDRESS = "http://localhost:3000/";
export const TABLE_NAME_OF_FAVORITES = "favorites";
export const TABLE_NAME_OF_COUNTRIES = "countries";
