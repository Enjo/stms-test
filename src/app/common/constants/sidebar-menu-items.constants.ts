export enum MENU_ITEMS {
  PROVIDERS,
  FAVORITES
}

export const MAIN_SIDEBAR_MENU_ITEMS = [
  {
    id: MENU_ITEMS.PROVIDERS,
    title: 'Providers',
    path: 'providers'
  },
  {
    id: MENU_ITEMS.FAVORITES,
    title: 'Favorites',
    path: 'favorites'
  }
];




