import { PROVIDER } from "./providers.enum";
import { FAKE_SERVER_ADDRESS, TABLE_NAME_OF_COUNTRIES } from './app.constants';

export const PROVIDER_DESC = {
  [PROVIDER.IMDB]: {
    title: "IMDB",
    api_link: "http://www.omdbapi.com/?i=tt3896198&apikey=634c88cf",
    table_columns: ["title", "year", "poster"],
    fields_for_favorites: {
      icon_url: "https://img.icons8.com/cotton/64/000000/cinema-.png",
      prefix: "imdb-",
      name: "title"
    }
  },
  [PROVIDER.COUNTRY]: {
    title: "Favorites Countries",
    // api_link: "http://universities.hipolabs.com/search?name=college",
    api_link: FAKE_SERVER_ADDRESS + TABLE_NAME_OF_COUNTRIES,
    table_columns: ["name", "region", "area", "currency_name", "flag"],
    fields_for_favorites: {
      icon_url: "https://img.icons8.com/offices/30/000000/country.png",
      prefix: "cntr-",
      name: "name"
    }
  }
};
