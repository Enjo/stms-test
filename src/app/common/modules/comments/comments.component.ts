import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-comments",
  templateUrl: "./comments.component.html",
  styleUrls: ["./comments.component.scss"]
})
export class CommentsComponent {
  commentsData = [];

  @Input() set arrayComments(val) {
    this.commentsData = val;
  }

  @Output() commentCreate = new EventEmitter<any>();

  onCommentCreate(text) {
    this.commentCreate.emit({ text: text });
  }
}
