import {
  Component,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from "@angular/core";

@Component({
  selector: "app-comment-create",
  templateUrl: "./comment-create.component.html",
  styleUrls: ["./comment-create.component.scss"]
})
export class CommentCreateComponent {
  @Output() created = new EventEmitter<any>();

  @ViewChild("createCommentInput", { static: false }) input: ElementRef;

  onAddComment(text: string) {
    if (text) {
      this.created.emit(text);
      this.input.nativeElement.value = "";
    }
  }
}
