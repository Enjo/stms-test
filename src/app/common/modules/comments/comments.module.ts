import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CommentsListComponent } from "./comments-list/comments-list.component";
import { CommentCreateComponent } from "./comment-create/comment-create.component";
import { CommentsComponent } from "./comments.component";
import { FormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    CommentsComponent,
    CommentsListComponent,
    CommentCreateComponent
  ],
  imports: [CommonModule, FormsModule, MatInputModule, PipesModule],
  exports: [CommentsComponent]
})
export class CommentsModule {}