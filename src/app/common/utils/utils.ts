export default class Utils {
  static searchInArray = (array: any, key: string, str: string) => {
    return array.filter(item => (item[key].toLowerCase()).includes(str.toLowerCase()));
  }

  static makeArrayById(array: any, key = 'id') {
    return array.reduce((acc, iteration) => {
      acc[iteration[key]] = iteration;
      return acc;
    }, {});
  }
}
