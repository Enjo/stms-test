import { NgModule } from "@angular/core";
import { TimeAgoPipe } from "./timeAgo.pipe";

@NgModule({
  declarations: [TimeAgoPipe],
  exports: [TimeAgoPipe]
})
export class PipesModule {}
