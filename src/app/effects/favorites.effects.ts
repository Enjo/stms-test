import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { map, switchMap, catchError } from 'rxjs/operators';

import * as fromServices from '../services/favorites.services';
import { FAVORITES_ACTIONS, EditFavorite } from '../store/actions/favorites.actions';
import { FAVORITE } from './../models/favorites.model';

@Injectable()
export class FavoritesEffects {
  constructor(
    private actions$: Actions,
    private favoritesService: fromServices.FavoritesService
  ) {}

  // @Effect() 
}