import {
  AfterContentInit,
  AfterViewInit,
  Component,
  OnInit,
  ViewChild
} from "@angular/core";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import { FavoritesService } from "src/app/services/favorites.services";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/app.state";
import { FAVORITE } from "src/app/models/favorites.model";
import { COMMENT } from "./../../models/comment.model";

@Component({
  selector: "app-favourite",
  templateUrl: "./favourite.component.html",
  styleUrls: ["./favourite.component.scss"]
})
export class FavouriteComponent implements OnInit, AfterContentInit {
  displayedColumns: string[] = ["icon", "title"];
  resultsLength = 0;
  isShowTable: boolean = false;
  isLoadingResults: boolean = true;

  dataSource = new MatTableDataSource([]);

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private service: FavoritesService,
    private store: Store<AppState>
  ) {}

  ngOnInit() {}
  ngAfterContentInit() {
    this.service.loadFavorites();

    this.store.select("favoritesPage").subscribe(data => {
      this.resultsLength = data.favorites.length;
      if (this.resultsLength === 0) {
        this.isShowTable = false;
        return;
      }

      this.isShowTable = true;
      this.isLoadingResults = false;

      this.dataSource = new MatTableDataSource(data.favorites);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  onCommentCreate(item: FAVORITE, value: any) {
    const comment: COMMENT = {
      id: 0,
      text: value.text,
      created_at: Math.floor(Date.now() / 1000)
    };

    item.comments.push(comment);
    this.service.editFavorite(item);
  }
}
