import { Component, OnInit, AfterViewInit, ViewChild } from "@angular/core";
import { merge, zip } from "rxjs";
import { map } from "rxjs/operators";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import { IMDB } from "src/app/models/imdb.model";
import { Select } from "src/app/models/select.model";
import { PROVIDER_DESC } from "src/app/common/constants/providers.constants";
import { PROVIDER } from "src/app/common/constants/providers.enum";
import Mappers from "src/app/common/mappers/mappers";
import Utils from "src/app/common/utils/utils";

import { GistService } from "src/app/services/gist.service";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/app.state";
import { FAVORITE } from "./../../models/favorites.model";
import { FavoritesService } from "src/app/services/favorites.services";

@Component({
  selector: "app-provider",
  templateUrl: "./provider.component.html",
  styleUrls: ["./provider.component.scss"],
  providers: [GistService]
})
export class ProviderComponent implements OnInit, AfterViewInit {
  tableData: IMDB[] = [];
  tableDataClone: IMDB[] = [];
  selectProviders: Select[];

  currentSelectedProvider = null;
  favoritesByProviderId = {};

  searchValue = "any";
  displayedColumns: string[];
  displayedColumnsBasic: string[] = ["checkbox"];
  resultsLength = 0;
  isShowTable: boolean = false;
  isLoadingResults: boolean = true;
  isRateLimitReached: boolean = false;
  isDisabledSearchInput: boolean = true;

  dataSource = new MatTableDataSource([]);

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    public gist: GistService,
    private service: FavoritesService,
    private store: Store<AppState>
  ) {
    // generate values for select
    this.selectProviders = Object.keys(PROVIDER_DESC).map(key => {
      return { value: Number(key), viewValue: PROVIDER_DESC[key].title };
    });

    this.store.select("favoritesPage").subscribe(data => {
      this.favoritesByProviderId = Utils.makeArrayById(
        data.favorites,
        "resource_id"
      );
    });
  }

  ngOnInit() {}
  ngAfterViewInit() {
    // this.dataSource.sortingDataAccessor = (data, header) => data[header];
    // this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  selectProviderEvent(e: number) {
    this.isShowTable = true;
    switch (e) {
      case PROVIDER.IMDB:
        // change columns in table
        this.displayedColumns = this.displayedColumnsBasic.concat(
          PROVIDER_DESC[PROVIDER.IMDB].table_columns
        );
        // get all items of list
        this.gist
          .getIMDBData('&s="' + this.searchValue + '"')
          .subscribe((resp: any) => {
            const observables = [];
            const amount = Math.ceil(resp.totalResults / 10);

            for (let i = 1; i <= amount; i++) {
              observables.push(
                this.gist
                  .getIMDBData('&s="' + this.searchValue + '"&page=' + i)
                  .pipe(
                    map((x: any) => {
                      return Mappers.imdbMapper(x.Search);
                    })
                  )
              );
            }
            // get all results
            zip(...observables).subscribe(x => {
              // flat array
              this.tableData = [].concat(...x);
              // copy array for search
              this.tableDataClone = JSON.parse(JSON.stringify(this.tableData));

              this.resultsLength = this.tableData.length;
              this.isLoadingResults = false;
              this.isDisabledSearchInput = false;

              this.dataSource = new MatTableDataSource(this.tableData);
              this.dataSource.sortingDataAccessor = (data, header) =>
                data[header];
              this.dataSource.sort = this.sort;
            });
          });
        this.currentSelectedProvider = PROVIDER.IMDB;
        break;

      case PROVIDER.COUNTRY:
        // change columns in table
        this.displayedColumns = this.displayedColumnsBasic.concat(
          PROVIDER_DESC[PROVIDER.COUNTRY].table_columns
        );
        // get data
        this.gist.getCountriesData().subscribe((resp: any) => {
          this.tableData = resp;
          // copy array for search
          this.tableDataClone = JSON.parse(JSON.stringify(this.tableData));

          this.resultsLength = resp.length;
          this.isLoadingResults = false;
          this.isDisabledSearchInput = false;

          this.dataSource = new MatTableDataSource(resp);
          this.dataSource.sortingDataAccessor = (data, header) => data[header];
          this.dataSource.sort = this.sort;
        });
        this.currentSelectedProvider = PROVIDER.COUNTRY;
        break;

      default:
        throw new Error();
    }
  }

  checkboxChange(e, row) {
    if (e.checked) {
      const favorite: FAVORITE = {
        id: 0, // fake id
        icon:
          PROVIDER_DESC[this.currentSelectedProvider].fields_for_favorites
            .icon_url,
        title:
          row[
            PROVIDER_DESC[this.currentSelectedProvider].fields_for_favorites
              .name
          ],
        resource_id:
          PROVIDER_DESC[this.currentSelectedProvider].fields_for_favorites
            .prefix + row.id,
        comments: []
      };

      this.service.addFavorite(favorite);
    } else {
      this.service.deleteFavorite(
        this.favoritesByProviderId[
          PROVIDER_DESC[this.currentSelectedProvider].fields_for_favorites
            .prefix + row.id
        ]
      );
    }
  }

  onSearchChange(value: string) {
    if (value === "") {
      this.tableData = this.tableDataClone;
    } else {
      this.tableData = Utils.searchInArray(
        this.tableDataClone,
        PROVIDER_DESC[this.currentSelectedProvider].fields_for_favorites.name,
        value
      );
    }
    this.dataSource = new MatTableDataSource(this.tableData);
    this.dataSource.sortingDataAccessor = (data, header) => data[header];
    this.dataSource.sort = this.sort;
  }

  isFavorite(id) {
    return this.favoritesByProviderId[
      PROVIDER_DESC[this.currentSelectedProvider].fields_for_favorites.prefix +
        id
    ]
      ? true
      : false;
  }
}
