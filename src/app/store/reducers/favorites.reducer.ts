import {
  FAVORITES_ACTIONS,
  FavoritesAction
} from "../actions/favorites.actions";

export const initialState = {
  favorites: []
};

export function favoritesReducer(
  state = initialState,
  action: FavoritesAction
) {
  switch (action.type) {
    case FAVORITES_ACTIONS.LOAD_ALL: {
      return {
        ...state,
        favorites: [...action.payload]
      };
    }
    case FAVORITES_ACTIONS.ADD_ONE: {
      return {
        ...state,
        favorites: [...state.favorites, action.payload]
      };
    }
    case FAVORITES_ACTIONS.EDIT_ONE: {
      return {
        ...state,
        favorites: [...state.favorites]
      };
    }
    case FAVORITES_ACTIONS.REMOVE_ONE: {
      return {
        ...state,
        favorites: [...state.favorites.filter(x => x.id !== action.payload.id)]
      };
    }
    default:
      return state;
  }
}
