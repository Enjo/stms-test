import { Action } from "@ngrx/store";
import { FAVORITE } from "../../models/favorites.model";

export namespace FAVORITES_ACTIONS {
  export const LOAD_ALL = "[FAVORITES] Load All";
  export const ADD_ONE = "[FAVORITES] Add One";
  export const EDIT_ONE = "[FAVORITES] Edit One";
  export const REMOVE_ONE = "[FAVORITES] Remove One";
}
export class LoadFavorites implements Action {
  readonly type = FAVORITES_ACTIONS.LOAD_ALL;
  constructor(public payload: FAVORITE[]) {}
}

export class AddFavorite implements Action {
  readonly type = FAVORITES_ACTIONS.ADD_ONE;
  constructor(public payload: FAVORITE) {}
}

export class EditFavorite implements Action {
  readonly type = FAVORITES_ACTIONS.EDIT_ONE;
  constructor(public payload: FAVORITE) {}
}

export class RemoveFavorite implements Action {
  readonly type = FAVORITES_ACTIONS.REMOVE_ONE;
  constructor(public payload: FAVORITE) {}
}

export type FavoritesAction = LoadFavorites | AddFavorite | RemoveFavorite | EditFavorite;
