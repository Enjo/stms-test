import { COMMENT } from "./comment.model";

export interface FAVORITE {
  id: number;
  icon: string;
  title: string;
  resource_id: string;
  comments: COMMENT[];
}
