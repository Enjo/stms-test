export interface COMMENT {
  id: number;
  text: string;
  created_at: number;
}
