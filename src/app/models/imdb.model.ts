export interface IMDB {
  title: string;
  year: string;
  poster: string;
  id: string;
}
