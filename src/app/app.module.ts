import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { AppRoutingModule } from "../app-routing/app-routing.module";
import { AppComponent } from "./app.component";
import { ProviderComponent } from "./components/provider/provider.component";
import { FavouriteComponent } from "./components/favourite/favourite.component";
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { FormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { MatProgressSpinnerModule } from "@angular/material";
import { HttpClientModule } from "@angular/common/http";
import { StoreModule } from "@ngrx/store";
import { FavoritesService } from "./services/favorites.services";
import { favoritesReducer } from "./store/reducers/favorites.reducer";
import { CommentsModule } from "./common/modules/comments/comments.module";
import { MatBadgeModule } from "@angular/material/badge";
import { MatSortModule } from "@angular/material/sort";

@NgModule({
  declarations: [
    AppComponent,
    ProviderComponent,
    FavouriteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSelectModule,
    MatInputModule,
    MatPaginatorModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatBadgeModule,
    MatTableModule,
    MatSortModule,
    CommentsModule,
    StoreModule.forRoot({ favoritesPage: favoritesReducer })
  ],
  providers: [FavoritesService],
  bootstrap: [AppComponent]
})
export class AppModule {}
