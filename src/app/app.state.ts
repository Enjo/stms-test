import { FAVORITE } from "./models/favorites.model";

export interface AppState {
  favoritesPage: {
    favorites: FAVORITE[];
    favoritesByProviderId: {}
  };
}
