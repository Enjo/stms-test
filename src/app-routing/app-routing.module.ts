import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProviderComponent} from '../app/components/provider/provider.component';
import {FavouriteComponent} from '../app/components/favourite/favourite.component';

const routes: Routes = [
  { path: '', redirectTo: '/providers', pathMatch: 'full' },
  {path: 'providers', component: ProviderComponent},
  {path: 'favorites', component: FavouriteComponent},
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
